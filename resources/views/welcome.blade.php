<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{ asset('image/fav.png') }}" rel="shortcut icon"/>

    <title>Coera</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Work+Sans" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<style type="text/css">
    body{
        font-family: 'Work Sans'; 
    }

    .redes{
        display: inline !important;
        margin-right: 20px
    }

    h1 { 
        font-size: 35px;
        color: #fff;
        font-weight: 400;

    }

    .laranja{

        background: #EB7300;
        color: #fff;
    }

    h4 {
        font-size: 18px;
        color: #fff;
    }

    .navbar-brand {
        margin-top: 10px;
        margin-bottom: 10px;
    }

    .botao{
        color: #fff;
        background: #B4D750;
        border-radius: 10px;
        padding: 2px 20px;
        font-size: 18px;

    }

    .mt{
        margin-top: 60px;
    }

    .padding{
        padding: 3.4rem 0;
    }

    h5 {
        font-size: 14px;  
    }

    .btn:hover {
        color: #000;

    }

    @media (min-width: 1200px){
        .container, .container-lg, .container-md, .container-sm, .container-xl {
            max-width: 800px;
        }
    }

    @media (max-width: 767px){
        h1 {
            font-size: 21px; 
            margin-top: 5px;
        }

        .img-logo{
            height: 180px;
        }

        h4 {
            font-size: 16px;
        }

        .botao{
            font-size: 14px;
            display: inline-block;
            margin-top: 20px;
        }
        .navbar-nav{
            display: flex !important;
        }
        .nav-link {
            display: inline;
            padding: 0px;
        }

        .padding{
            padding: 20px;
        }
    }
</style>
<body>
    <div class="container">
        <nav class="navbar navbar-expand-md navbar-light bg-white">
            <a class="navbar-brand" href="{{ url('/') }}">
                <img src="{{ asset('image/logo.png') }}">
            </a>
        </nav>
    </div>
    <div class="laranja">
        <div class="container padding">
            <div class="row" >
                <div class="col-md-12 col-12">
                    <h1>
                        Auxiliamos as organizações a <br>
                        ampliarem a sua geração de valor<br>
                        e impacto positivo através<br>
                        da sustentabilidade.  
                    </h1>
                </div>
            </div>
            <div class="row mt" >
                <div class="col-md-7">
                    <h4 >
                        Vanessa Cabral Gomes<br>
                        Consultora Sênior<br>
                        (51) 9 9900.6217<br>
                        vane@coera.com.br
                    </h4>
                </div>
                <div class="col-md-5">

                    <a class="btn botao btn-lg " href="{{ asset('Coera_ApresentaçãoServiços_2020.pdf') }}" download role="button">Conheça nossos serviços | PDF</a>


                    
                    <br><br>

                    <a class=" redes" href="https://www.linkedin.com/company/coerasustentabilidade/" target="_blank"><img src="{{ asset('image/linkedIn.png') }}" > </a>

                    <a class="redes" href="https://www.instagram.com/coerasustentabilidade/" target="_blank">
                        <img src="{{ asset('image/insta.png') }}">
                    </a> 

                </div>
            </div>
            <div class="row mt">
                <div class="col-md-12">
                    <h5>
                        Site em desenvolvimento.
                    </h5>
                </div>
            </div>
        </div>
    </div>
    <script
    src="https://code.jquery.com/jquery-3.5.1.min.js"
    integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
    crossorigin="anonymous"></script>
    <script type="text/javascript">
        var h = $( window ).height();
        typeof h;

        var altura = h - 119.22;
        typeof altura;

       // alert($( window ).width() > 768);

        if($( window ).width() > 768){
             $('.laranja').height(altura);
        }
       
    </script>
</body>
</html>
